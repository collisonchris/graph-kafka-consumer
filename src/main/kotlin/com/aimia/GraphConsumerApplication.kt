package com.aimia

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class GraphConsumerApplication
    fun main(args: Array<String>) {
        runApplication<GraphConsumerApplication>(*args)
    }

