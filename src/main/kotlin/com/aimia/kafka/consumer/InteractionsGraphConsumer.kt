package com.aimia.kafka.consumer

import com.aimia.neo4j.InteractionDao
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.support.KafkaHeaders
import org.springframework.messaging.handler.annotation.Header
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Service

@Service
class InteractionsGraphConsumer {
    @Autowired
    lateinit var objectMapper : ObjectMapper

    @Autowired
    lateinit var interactionDao: InteractionDao


    @KafkaListener(topics = ["alp-e-interactions"], groupId = "alp-etl-user-19", containerFactory = "customKafkaListenerContainerFactory")
    fun listen(@Payload message: String, @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) key : Int) {
        println("Key: $key")
        interactionDao.mergeInteraction(objectMapper.readValue(message) as Map<String,Any>)
    }
}