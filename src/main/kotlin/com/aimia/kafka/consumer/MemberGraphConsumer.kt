package com.aimia.kafka.consumer

import com.aimia.neo4j.MemberDao
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.kafka.annotation.KafkaListener
import org.springframework.kafka.support.KafkaHeaders
import org.springframework.messaging.handler.annotation.Header
import org.springframework.messaging.handler.annotation.Payload
import org.springframework.stereotype.Service

@Service
class MemberGraphConsumer {

    @Autowired
    lateinit var objectMapper : ObjectMapper

    @Autowired
    lateinit var memberDao : MemberDao

    @KafkaListener(topics = ["alp-e-members"], groupId = "alp-etl-user-19", containerFactory = "customKafkaListenerContainerFactory")
    fun listen(@Payload message: String, @Header(KafkaHeaders.RECEIVED_MESSAGE_KEY) key : Int) {
        println("Key: $key Message: $message")
        memberDao.mergeMember(objectMapper.readValue(message) as Map<String, Any>)
    }


}