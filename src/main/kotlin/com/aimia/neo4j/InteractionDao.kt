package com.aimia.neo4j

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.readValue
import org.neo4j.driver.Driver
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class InteractionDao {

    companion object {
        val baseMemberInteractionQuery = "MERGE (i:Interaction{id: \$interaction_id}) "
    }
    @Autowired
    lateinit var neo4jDriver : Driver

    @Autowired
    lateinit var objectMapper : ObjectMapper

    fun mergeInteraction(interactionMap: Map<String, Any>) {
        //we need to create a node for the Interaction payload & draw the relationship to the member

        //parse payload
        var payload = objectMapper.readValue(interactionMap.get("interaction_payload") as String) as MutableMap<String,Any>
        payload.remove("capabilities")
        payload.remove("id")
        payload.remove("interactionId")
        var keyPairsString = parsePayloadAndCreateKeyValuePairs(payload)

        var memberInteractionQuery = createInteractionQuery(keyPairsString)

        payload["interaction_id"] = interactionMap.get("interaction_id") as Int

        neo4jDriver.session().runCatching {
            this.run(memberInteractionQuery,payload)
        }.onSuccess {
            println("merged Interaction with id: ${interactionMap["interaction_id"]}")
        }.onFailure {
            throw it
        }
        //now create a relationship
        neo4jDriver.session().runCatching {
            val memberInteractionRelationshipQuery = "MATCH (m:Member{id: ${interactionMap["member_id"]}}), (i:Interaction{id: ${interactionMap["interaction_id"]}}) MERGE (m)-[r:HAS_INTERACTION {name: '${interactionMap["interaction_type"]}', interaction_date: '${interactionMap["interaction_date"]}' }]->(i)"
            this.run(memberInteractionRelationshipQuery)
        }.onSuccess {
            println("built relationship from Interaction with id: ${interactionMap["interaction_id"]} to member_id ${interactionMap["member_id"]}")
        }.onFailure {
            throw it
        }
    }

    private fun createInteractionQuery(keyPairsString: String) =
        baseMemberInteractionQuery + " ON CREATE SET " + keyPairsString + " ON MATCH SET " + keyPairsString

    private fun parsePayloadAndCreateKeyValuePairs(interactionPayload: Map<String,Any>) : String {
        return createKeyValuePairs("i", interactionPayload)
    }

    private fun createKeyValuePairs(prefixVariable: String, values : Map<String, Any>) : String {
        var result = StringBuilder()
        var mapSize = values.size
        var counter = 1
        values.forEach{ (key, _) ->
            if( key != "capabilities" && key != "id") {
                result.append(prefixVariable)
                result.append(".")
                result.append(key)
                result.append("=")
                result.append("$")
                result.append(key)
                if(counter < mapSize) {
                    result.append(",")
                }
            }
            counter++
        }
        return result.toString()
    }
}