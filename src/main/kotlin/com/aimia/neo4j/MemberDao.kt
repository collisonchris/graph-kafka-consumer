package com.aimia.neo4j

import org.neo4j.driver.Driver
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class MemberDao {

    companion object {
        val memberMergeQuery = "MERGE (m:Member{id: \$id}) " +
                "ON CREATE SET m.first_name = \$first_name, m.last_name = \$last_name, m.gender = \$gender, m.status_code = \$status_code, m.enrollment_date = \$enrollment_date, m.program_code = \$program_code, m.suffix = \$suffix, m.date_of_birth = \$date_of_birth " +
                "ON MATCH SET m.first_name = \$first_name, m.last_name = \$last_name, m.gender = \$gender, m.status_code = \$status_code, m.enrollment_date = \$enrollment_date, m.program_code = \$program_code, m.suffix = \$suffix, m.date_of_birth = \$date_of_birth"
    }
    @Autowired
    lateinit var neo4jDriver : Driver

    fun mergeMember(memberMap: Map<String, Any>) {

        neo4jDriver.session().runCatching {
            this.run(memberMergeQuery,memberMap)
        }.onSuccess {
            println("merged member with id: ${memberMap["id"]}")
        }.onFailure {
            throw it
        }

    }

}