package com.aimia.neo4j

import org.neo4j.driver.Driver
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Repository

@Repository
class TransactionDao {
    companion object {
        val transactionMergeQuery = "MERGE (t:Transaction{id: \$id}) " +
                "ON CREATE SET t.interaction_id = \$interaction_id, t.currency_code = \$currency_code, t.amount = \$amount, t.transaction_type = \$transaction_type, t.issuing_partner = \$issuing_partner, t.transaction_date = \$transaction_date, t.posting_date = \$posting_date " +
                "ON MATCH SET t.interaction_id = \$interaction_id, t.currency_code = \$currency_code, t.amount = \$amount, t.transaction_type = \$transaction_type, t.issuing_partner = \$issuing_partner, t.transaction_date = \$transaction_date, t.posting_date = \$posting_date  "
    }

    @Autowired
    lateinit var neo4jDriver : Driver

    fun mergeTransaction(transactionMap: Map<String, Any>) {
        neo4jDriver.session().runCatching {
            this.run(transactionMergeQuery,transactionMap)
        }.onSuccess {
            println("merged Transaction with id: ${transactionMap["id"]}")
        }.onFailure {
            throw it
        }

        //draw relationship to interaction_id
        neo4jDriver.session().runCatching {
            val memberInteractionRelationshipQuery = "MATCH (i:Interaction{id: ${transactionMap["interaction_id"]}}), (t:Transaction{id:${transactionMap["id"]} }) MERGE (i)-[r:HAS_TRANSACTION {name: '${transactionMap["transaction_type"]}' }]->(t)"
            this.run(memberInteractionRelationshipQuery)
        }.onSuccess {
            println("built relationship from Interaction with id: ${transactionMap["interaction_id"]} to Transaction ${transactionMap["id"]}")
        }.onFailure {
            throw it
        }
    }
}

//mutableMapOf("id" to rs.getInt("id"),
//"interaction_id" to rs.getInt("interaction_id"),
//"currency_code" to rs.getString("code"),
//"amount" to rs.getInt("amount"),
//"transaction_type" to rs.getString("transaction_type"),
//"issuing_partner" to rs.getString("issuing_partner"),
//"transaction_date" to rs.getDate("transaction_date"),
//"posting_date" to rs.getDate("posting_date"))